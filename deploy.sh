#!/bin/bash
#
# ./deploy.sh - Appelle le playbook principal qui gère l'infrastructure de
#               l'escale, avec les variables chargées à partir de config.yml.

set -euo pipefail

VERBOSE=${1:-}

VERB_ARG="-vv"
if [ "$VERBOSE" == "--verbose" ] || [ "$VERBOSE" == "-v" ]; then
  VERB_ARG="-vvvv"
fi

ansible-playbook --ask-vault-password -e @vault.yml -e @config.yml playbooks/escale.yml "$VERB_ARG" "$@"
