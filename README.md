# Infrastructure de l'Escale & CO

https://escalenumerique.fr

## Contribuer

Vous avez remarqué un problème ? Ou vous voulez ajouter une fonctionalité ?
Regarder sur notre dépôt si une [_issue_][issues] existe déjà, et si non n'hésitez
pas à [en ouvrir une][new_issue].

Pour contribuer, il vous faudra avoir quelques connaissances de Git, Linux,
Debian, et [Ansible][ansible].

[issues]: https://codeberg.org/escale-and-co/infra/issues
[new_issue]: https://codeberg.org/escale-and-co/issues/new
[ansible]: https://docs.ansible.com

Le projet utilise `ansible-lint` (installé via le paquet `ansible`, et qui
utilise aussi `yamllint`) comme _linter_. Il sera appliqué automatiquement
lorsque nous aurons accès au _CI_ (_Continuous Integration_) de Codeberg.
