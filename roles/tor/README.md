# Tor

Ce rôle configure des vhosts onion.

## Variables

- `vhosts` variable, when called by another role

Propriétés d'un vhost:
- `name`: Requis. Identifiant pour le vhost onion, généralement nom de domaine qualifié.
- `ports`: Requis. Un dictionaire de ports à proxy vers une addresse / port spécifique.

## Conventions

In order to interface with other roles/services, this role follows a certain number of conventions:

- onion directories are placed in `/var/lib/tor/IDENTIFIER`, where `IDENTIFIER` is the vhost's name
- `/var/lib/tor/IDENTIFIER/hostname` contains the generated onion domain
