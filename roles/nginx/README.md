# Nginx

Installation et configuration de nginx, ainsi que de virtualhosts selon
différent modèles.

Ce rôle dépend du rôle 'tls' pour la génération de certificats.

## Variables

`vhosts`: Liste de Virtual Hosts. Un Virtual Host correspond à un fichier de
configuration nginx dans /etc/nginx/sites-available.

Propriétés d'un vhost:
- `name`: Requise. Nom de domaine du Virtual Host.
- `enabled`: Optionel. `true` par défaut. Activé ou non. (Lien symbolique dans
  /etc/nginx/sites-enabled).
- `template`: Optionel. Modèle à utiliser pour générer le fichier de
  configuration. À combiner avec `content`.
- `content`: Optionel. Contenu du fichier de configuration, si utilisé seul
  sans `template`, sinon contenu partiel du fichier si utilisé avec le modèle
  "partial".
- `order`: Optionel. Prefix à ajouter au nom du fichier de configuration pour
  une configuration plus précise.
- `tls`: Optionel. `true` par défaut. Appelle le rôle 'tls' pour générer un
  certificate pour le domaine en question.
- `aliases`: Optionel. Liste de nom de domaines sur lesquels le Virtual Host
  écoutera aussi.

## Utilisation

```yaml
- name: 'Création du vhost Nextcloud pour XXX'
  ansible.builtin.include_role:
    name: 'nginx'
  vars:
    vhosts:
      - name: 'XXX'
        enabled: false
        aliases: ['YYY', 'ZZZ']
        template: 'partial'
        content: "{{ lookup('file', 'files/nginx-nextcloud.conf') }}"
```
