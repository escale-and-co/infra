# Tls

Génération de certificats Tls via certbot. Ce rôle dépend du rôle 'nginx' qui
configure le virtualhost par défaut pour que les challenges Let's Encrypt
soient accessibles.

## Variables

`le_email`: Requis. Email associé au compte Let's Encrypt.
`tls_domain`: Domaine principal, CN du certificat  
`tls_aliases`: Optionel. Liste de domaines secondaires, ajouté en tant que Subject Alternative Names au certificat.

## Utilisation

```yaml
- name: 'Génération du certificat pour XXX'
  ansible.builtin.include_role:
    name: 'tls'
  vars:
    tls_domain: 'XXX'
    tls_aliases:
      - 'YYY'
      - 'ZZZ'
```
