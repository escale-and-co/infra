# Base

Configure un système vierge selon nos attentes (paquets, accès, root, etc.).

## Variables

`packages`: Optionel. Liste de paquets à installer
`root_password`: Requis. Mot de passe root.
`users`: Requis. Utilisateurices avec des permissions root.

Propriété d'un "user":
- `name`: Requis. Nom de l'utilisateurice.
- `keys`. Requis. Liste de clé SSH à configurer.
- `comment`: Optionel. La description d'une utilisateurice (aka GECOS).
