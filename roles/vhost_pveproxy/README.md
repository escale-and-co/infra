# vhost_pveproxy

Configure nginx pour servir pveproxy sur le port tls par défaut, avec un
certificat TLS valide pour le domaine spécifié.

## Variables

`hostname`: Domaine sur lequel pveproxy sera servi.
